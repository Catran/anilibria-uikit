//
//  ReleasesResponse.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct ReleasesResponseWrapped: Decodable {
    let data: ReleasesResponse?
}

struct SearchResponseWrapped: Decodable {
    let data: [ReleaseResponse]
}

struct ReleasesResponse: Decodable {
    let items: [ReleaseResponse]
}

struct ReleaseResponseWrapped: Decodable {
    let data: ReleaseResponse?
}

struct ListResponse: Decodable {
    let data: [String]
}

struct ReleaseResponse: Decodable {
    let id: Int?
    let code: String?
    let names: [String]?
    let series: String?
    let description: String?
    let poster: String?
    let status: String?
    let type: String?
    let genres: [String]?
    let voices: [String]?
    let year: String?
    let day: String?
    let playlist: [SeriesResponse]?
}

struct SeriesResponse: Decodable {
    let id: Int
    let title: String
    let sd: String
    let hd: String
    let fullhd: String?
    let srcSd: String?
    let srcHd: String?
}
