//
//  NetworkDataFetcher.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol DataFetcher {
    func getReleases(filter: FilterViewModelType, page: Int, response: @escaping (ReleasesResponse?) -> Void)
    func getReleases(title: String, page: Int, response: @escaping ([ReleaseResponse]?) -> Void)
    func getRelease(id: Int, response: @escaping (ReleaseResponse?) -> Void)
    func getList(title: String, response: @escaping ([String]?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {
    
    let networking: Networking
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func getReleases(filter: FilterViewModelType, page: Int = 1, response: @escaping (ReleasesResponse?) -> Void) {
        
        let finish = filter.isFinishedRelease ? "2" : "1"
        let sort = filter.sort == "1" ? "2" : "1"
        let perPage = 12
        let filters: [Filter] = [.names, .description, .poster]
        
        var params = "query=catalog&perPage=\(perPage)&page=\(page)&sort=\(sort)&xpage=catalog&finish=\(finish)"
        params += getSearchString(years: filter.years, genres: filter.genres, seasons: filter.seasons)
        
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        networking.request(params: data) { (data, error) in
            if let error = error {
                print("Error received requesting data: \(error.localizedDescription)")
                response(nil)
            }
            
            let decoded = self.decodeJSON(type: ReleasesResponseWrapped.self, from: data)
            response(decoded?.data)
        }
    }
    
    func getReleases(title: String, page: Int = 1, response: @escaping ([ReleaseResponse]?) -> Void) {
        let perPage = 12
        let filters: [Filter] = [.names, .description, .poster]
        
        var params = "query=search&search=\(title)&perPage=\(perPage)&page=\(page)"
        
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        networking.request(params: data) { (data, error) in
           if let error = error {
               print("Error received requesting data: \(error.localizedDescription)")
               response([])
           }
           
           let decoded = self.decodeJSON(type: SearchResponseWrapped.self, from: data)
            response(decoded?.data)
        }
    }
    
    func getRelease(id: Int, response: @escaping (ReleaseResponse?) -> Void) {
        let params = "query=release&id=\(id)"
        guard let data = params.data(using: .utf8) else { return }
        networking.request(params: data) { (data, error) in
           if let error = error {
               print("Error received requesting data: \(error.localizedDescription)")
               response(nil)
           }
           
           let decoded = self.decodeJSON(type: ReleaseResponseWrapped.self, from: data)
            response(decoded?.data)
        }
    }
    
    func getList(title: String, response: @escaping ([String]?) -> Void) {
        let params = "query=\(title)"
        
        guard let data = params.data(using: .utf8) else { return }
        networking.request(params: data) { (data, error) in
           if let error = error {
               print("Error received requesting data: \(error.localizedDescription)")
               response([])
           }
           
           let decoded = self.decodeJSON(type: ListResponse.self, from: data)
            response(decoded?.data)
        }
    }
    
    
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = from else { return nil }
        
        do {
            let response = try decoder.decode(type.self, from: data)
            return response
        } catch let err {
            print(err)
        }
        return nil
    }
    
    private func filterToString(_ filters: [Filter]) ->String {
        let names = filters.map { $0.rawValue }
        return names.joined(separator: ",")
    }
    
    private func getSearchString(years: String, genres: String, seasons: String)-> String {
        return "&search={\"genre\":\"" + genres.replacingOccurrences(of: ", ", with: ",") + "\", \"year\":\"" + years.replacingOccurrences(of: ", ", with: ",") + "\", \"season\":\"" + seasons.replacingOccurrences(of: ", ", with: ",") + "\"}"
    }
}

//MARK: - Extensions -
extension NetworkDataFetcher {
    enum Sort: String {
        case newness = "1"
        case favorites = "2"
    }
}

extension NetworkDataFetcher {
    enum Filter: String {
        case id = "id"
        case code = "code"
        case names = "names"
        case series = "series"
        case poster = "poster"
        case favorite = "favorite"
        case last = "last"
        case moon = "moon"
        case status = "status"
        case type = "type"
        case genres = "genres"
        case voices = "voices"
        case year = "year"
        case day = "day"
        case description = "description"
        case playlist = "playlist"
    }
}
