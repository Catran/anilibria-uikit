//
//  NetworkService.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol Networking {
    func request(params body: Data, completion: @escaping (Data?, Error?) -> Void)
    func getImage(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

final class NetworkService: Networking {
    
    private static let ssid = "PCpN2a0HaEmjUFJ0QUB9dcCtt8"
    private static let url = URL(string: "https://www.anilibria.tv/public/api/index.php")!
    private var request: URLRequest = URLRequest(url: url)
    private var session: URLSession
    
    static var shared: NetworkService = {
        let instance = NetworkService()
        return instance
    }()
    
    private init() {
        self.request.httpMethod = "POST"
        self.request.addValue("PHPSESSID", forHTTPHeaderField: NetworkService.ssid)
        
        let config = URLSessionConfiguration.default
        config.connectionProxyDictionary = [AnyHashable: Any]()
        config.connectionProxyDictionary?["HTTPSProxy"] = "5.187.0.24"
        config.connectionProxyDictionary?["HTTPSPort"] = 3128
        
        self.session = URLSession(configuration: config)
    }
    
    func request(params body: Data, completion: @escaping (Data?, Error?) -> Void) {
        self.request.httpBody = body
        let task = createDataTask(completion: completion)
        task.resume()
    }
    
    func getImage(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask  {
        return self.session.dataTask(with: url, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                completion(data, response, error)
            }
        })
    }
    
    private func createDataTask(completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask {
        return self.session.dataTask(with: self.request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                completion(data, error)
            }
        })
    }
}

