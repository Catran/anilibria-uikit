//
//  FilterView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 07.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class FilterView: UIView {
    
    weak var viewModel: FilterViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            switchFinish.isOn = viewModel.isFinishedRelease
            textFieldSorting.text = viewModel.sort == "1" ? "по популярности" : "по новизне"
            let row = viewModel.sort == "1" ? 0 : 1
            pikerSorting.selectRow(row, inComponent: 0, animated: true)
            selectedTextYears.setTitle(viewModel.years.isEmpty ? "Все года..." : viewModel.years, for: .normal)
            selectedTextGenres.setTitle(viewModel.genres.isEmpty ? "Все жанры..." : viewModel.genres, for: .normal)
            selectedTextSeasons.setTitle(viewModel.seasons.isEmpty ? "Все сезоны..." : viewModel.seasons, for: .normal)
        }
    }
    
    let cardView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let labelFinish: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.systemGray
        view.text = "Заврешенные релизы"
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let switchFinish: UISwitch = {
        let view  = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.onTintColor = UIColor.Red.dark
        return view
    }()
    
    let labelSorting: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.systemGray
        view.text = "Сортировка"
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let textFieldSorting: UITextField = {
       let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = UIColor.Red.dark
        view.textAlignment = .right
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.tintColor = .clear
        return view
    }()
    
    let pikerSorting: UIPickerView = {
       let view = UIPickerView()
        return view
    }()
    
    let toolBarPickerSorting: UIToolbar = {
        let view = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
        view.tintColor = UIColor.Red.dark
        return view
    }()
    
    let labelYears: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.systemGray
        view.text = "Года"
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let selectedTextYears: UIButton = {
        let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitleColor(UIColor.Red.dark, for: .normal)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        view.accessibilityHint = "Года"
        view.accessibilityValue = "years"
        return view
    }()
    
    let labelGenres: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.systemGray
        view.text = "Жанры"
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let selectedTextGenres: UIButton = {
        let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitleColor(UIColor.Red.dark, for: .normal)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        view.accessibilityHint = "Жанры"
        view.accessibilityValue = "genres"
        return view
    }()
    
    let labelSeasons: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.systemGray
        view.text = "Сезоны"
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let selectedTextSeasons: UIButton = {
       let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitleColor(UIColor.Red.dark, for: .normal)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        view.accessibilityHint = "Сезоны"
        view.accessibilityValue = "seasons"
       return view
    }()
    
    let  buttonStartSearch: UIButton = {
       let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Найти анимешечку", for: .normal)
        view.setTitleColor(UIColor.white, for: .normal)
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        view.backgroundColor = UIColor.Red.dark
        view.layer.cornerRadius = 10
       return view
    }()
    
    let buttonResetFilter: UIButton = {
       let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("сбросить фильтр", for: .normal)
        view.setTitleColor(UIColor.systemGray, for: .normal)
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .callout)
       return view
    }()
    
    var pikerSortingHeightConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        overlayFirstLayer()
        overlaySecondLayer()
    }
    
    func overlaySecondLayer() {
        cardView.addSubview(labelFinish)
        cardView.addSubview(switchFinish)
        cardView.addSubview(labelSorting)
        cardView.addSubview(textFieldSorting)
        cardView.addSubview(labelYears)
        cardView.addSubview(selectedTextYears)
        cardView.addSubview(labelGenres)
        cardView.addSubview(selectedTextGenres)
        cardView.addSubview(labelSeasons)
        cardView.addSubview(selectedTextSeasons)
        cardView.addSubview(buttonStartSearch)
        cardView.addSubview(buttonResetFilter)

        //  labelFinish constaraint
        labelFinish.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 10).isActive = true
        labelFinish.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        labelFinish.trailingAnchor.constraint(equalTo: switchFinish.leadingAnchor, constant: 8).isActive = true
        labelFinish.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  switchFinish constaraint
        switchFinish.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 10).isActive = true
        switchFinish.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        switchFinish.widthAnchor.constraint(equalToConstant: 60).isActive = true
        switchFinish.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  labelSorting constaraint
        labelSorting.topAnchor.constraint(equalTo: labelFinish.bottomAnchor, constant: 10).isActive = true
        labelSorting.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        labelSorting.trailingAnchor.constraint(equalTo: textFieldSorting.leadingAnchor, constant: 8).isActive = true
        labelSorting.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  textFieldSorting constaraint
        textFieldSorting.topAnchor.constraint(equalTo: labelFinish.bottomAnchor, constant: 10).isActive = true
        textFieldSorting.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        textFieldSorting.widthAnchor.constraint(equalToConstant: 150).isActive = true
        textFieldSorting.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  labelYears constaraint
        labelYears.topAnchor.constraint(equalTo: labelSorting.bottomAnchor, constant: 10).isActive = true
        labelYears.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        labelYears.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        labelYears.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  selectedTextYears constaraint
        selectedTextYears.topAnchor.constraint(equalTo: labelYears.bottomAnchor, constant: 10).isActive = true
        selectedTextYears.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        selectedTextYears.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        selectedTextYears.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  labelGenres constaraint
        labelGenres.topAnchor.constraint(equalTo: selectedTextYears.bottomAnchor, constant: 10).isActive = true
        labelGenres.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        labelGenres.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        labelGenres.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  selectedTextGenres constaraint
        selectedTextGenres.topAnchor.constraint(equalTo: labelGenres.bottomAnchor, constant: 10).isActive = true
        selectedTextGenres.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        selectedTextGenres.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        selectedTextGenres.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  labelSeasons constaraint
        labelSeasons.topAnchor.constraint(equalTo: selectedTextGenres.bottomAnchor, constant: 10).isActive = true
        labelSeasons.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        labelSeasons.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        labelSeasons.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  selectedTextSeasons constaraint
        selectedTextSeasons.topAnchor.constraint(equalTo: labelSeasons.bottomAnchor, constant: 10).isActive = true
        selectedTextSeasons.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        selectedTextSeasons.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        selectedTextSeasons.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //  buttonStartSearch constaraint
        buttonStartSearch.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        buttonStartSearch.bottomAnchor.constraint(equalTo: buttonResetFilter.topAnchor, constant: -10).isActive = true
        buttonStartSearch.widthAnchor.constraint(equalToConstant: 240).isActive = true
        buttonStartSearch.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        //  buttonResetFilter constaraint
        buttonResetFilter.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        buttonResetFilter.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -10).isActive = true
        buttonResetFilter.widthAnchor.constraint(equalToConstant: 160).isActive = true
        buttonResetFilter.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func overlayFirstLayer() {
        addSubview(cardView)
        
        //  cardView constaraint
        cardView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
