//
//  SearchCellTableViewCell.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

final class SearchCellTableViewCell: UITableViewCell {
    
    static let reuseId = "SearchCodeCell"
    private var task: URLSessionDataTask?
    
    weak var viewModel: SearchCellViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            releaseTitle.text = viewModel.title
            releaseDescription.text = viewModel.description
            if self.task == nil {
                task = releasePoster.set(imageURL: viewModel.posterUrl)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.task?.cancel()
        self.task = nil
        releasePoster.image = UIImage(named: "preloadPoster")
    }
    
    let cardView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let releasePoster: WebImageView = {
       let view = WebImageView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "preloadPoster")
        return view
    }()
    
    let releaseTitle: UILabel = {
       let view = UILabel()
        view.textColor = UIColor.Red.dark
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.adjustsFontForContentSizeCategory = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let releaseDescription: UILabel = {
       let view = UILabel()
        view.numberOfLines = 7
        view.font = UIFont.preferredFont(forTextStyle: .footnote)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        overlayFirstLayer()
        overlaySecondLayer()
    }
    
    func overlaySecondLayer() {
        cardView.addSubview(releasePoster)
        cardView.addSubview(releaseTitle)
        cardView.addSubview(releaseDescription)
        
        //  releasePoster constaraint
        releasePoster.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 5).isActive = true
        releasePoster.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        releasePoster.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -5).isActive = true
        releasePoster.widthAnchor.constraint(equalToConstant: 90).isActive = true

        //  releaseTitle constaraint
        releaseTitle.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 5).isActive = true
        releaseTitle.leadingAnchor.constraint(equalTo: releasePoster.trailingAnchor, constant: 10).isActive = true
        releaseTitle.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        releaseTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true

        //  releaseDescription constaraint
        releaseDescription.topAnchor.constraint(equalTo: releaseTitle.bottomAnchor, constant: 5).isActive = true
        releaseDescription.leadingAnchor.constraint(equalTo: releasePoster.trailingAnchor, constant: 10).isActive = true
        releaseDescription.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        releaseDescription.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -5).isActive = true
    }
    
    func overlayFirstLayer() {
        addSubview(cardView)
        
        //  constaraint
        cardView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
