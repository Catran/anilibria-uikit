//
//  NotFoundTableViewCell.swift
//  Anilibria
//
//  Created by Петр Яскевич on 07.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class NotFoundTableViewCell: UITableViewCell {

    static let reuseId = "NotFoundCodeCell"
    
    let cardView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageNotFound: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "notFoundPoster")
        return view
    }()

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        overlayFirstLayer()
        overlaySecondLayer()
    }
    
    func overlaySecondLayer() {
        cardView.addSubview(imageNotFound)
        
        //  releasePoster constaraint
        imageNotFound.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 5).isActive = true
        imageNotFound.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        imageNotFound.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -8).isActive = true
        imageNotFound.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -5).isActive = true
    }
    
    func overlayFirstLayer() {
        addSubview(cardView)
        
        //  constaraint
        cardView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
    }

}
