//
//  WebImageView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class WebImageView: UIImageView {
    
    private let rootUrl = "https://www.anilibria.tv"
    
    func set(imageURL: String?) -> URLSessionDataTask? {
        guard let imageURL = imageURL, let url = URL(string: rootUrl + imageURL) else {
            self.image = nil
            return nil
        }
        
        if let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: url)) {
            self.image = UIImage(data: cachedResponse.data)
            return nil
        }
        
        let task = NetworkService.shared.getImage(url: url) { [weak self]  (data, response, error) in
            if let data = data, let response = response {
                self?.image = UIImage(data: data)
                self?.handleLoadedImage(data: data, response: response)
            }
        }
        task.resume()
        return task
    }
    
    private func handleLoadedImage(data: Data, response: URLResponse) {
        guard let responseURL = response.url else { return }
        let cachedResponse = CachedURLResponse(response: response, data: data)
        URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: responseURL))
        
    }
}

