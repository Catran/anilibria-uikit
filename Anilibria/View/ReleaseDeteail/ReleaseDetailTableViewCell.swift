//
//  ReleaseDetailTableViewCell.swift
//  Anilibria
//
//  Created by Петр Яскевич on 04.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class ReleaseDetailTableViewCell: UITableViewCell {

    static let reuseId = "ReleaseDetailCodeCell"
    
    weak var viewModel: ReleaseDetailCellViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            title.text = viewModel.title
            content.text = viewModel.description
        }
    }
    
    let card: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let title: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textColor = UIColor.Red.dark
        label.textAlignment = .left
        label.numberOfLines = 0
        label.adjustsFontForContentSizeCategory = true
        return label
    }()
    
    let content: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = UIColor.secondaryLabel
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.adjustsFontForContentSizeCategory = true
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        overlayFirstLayer()
        overlaySecondLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func overlaySecondLayer() {
        card.addSubview(title)
        card.addSubview(content)
        
        // constraint
        title.topAnchor.constraint(equalTo: card.topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: card.leadingAnchor).isActive = true
        title.trailingAnchor.constraint(equalTo: card.trailingAnchor).isActive = true
        title.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        content.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 5).isActive = true
        content.leadingAnchor.constraint(equalTo: card.leadingAnchor).isActive = true
        content.trailingAnchor.constraint(equalTo: card.trailingAnchor).isActive = true
        content.bottomAnchor.constraint(equalTo: card.bottomAnchor).isActive = true
    }
    
    func overlayFirstLayer() {
        addSubview(card)
        
        // constraint
        card.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        card.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        card.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        card.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
    
}
