//
//  ReleaseWeekdayStatusTableViewCell.swift
//  Anilibria
//
//  Created by Петр Яскевич on 04.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class ReleaseWeekdayStatusTableViewCell: UITableViewCell {

        static let reuseId = "ReleaseWeekdayStatusTableViewCell"
    
        weak var viewModel: ReleaseDetailViewModelType? {
            willSet(viewModel) {
                guard let viewModel = viewModel else { return }
                let label = card.arrangedSubviews[viewModel.day - 1] as! UILabel
                label.backgroundColor = UIColor.Red.dark
                label.textColor = UIColor.white
            }
        }
        
        lazy var card: UIStackView = {
            let view = UIStackView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.axis = .horizontal
            view.distribution = .equalCentering
            view.alignment = .center
            view.spacing = 5
            return view
        }()
        

        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            overlayFirstLayer()
            overlaySecondLayer()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func overlaySecondLayer() {
            
            //  add to stackView
            let nomberOfDays = 7
            for i in 0 ..< nomberOfDays {
                let day = configLabel()
                day.text = setWeekday(index: i)
                card.addArrangedSubview(day)
            }
        }
        
        func overlayFirstLayer() {
            addSubview(card)
            
            // constraint
            card.topAnchor.constraint(equalTo: topAnchor).isActive = true
            card.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
            card.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
            card.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
    
        private func configLabel() -> UILabel {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.clipsToBounds = true
            label.layer.borderColor = UIColor.Red.dark.cgColor
            label.layer.borderWidth = 2
            label.layer.cornerRadius = 20
            label.backgroundColor = UIColor.clear
            label.font = UIFont.preferredFont(forTextStyle: .headline)
            label.textColor = UIColor.Red.dark
            label.textAlignment = .center
            label.numberOfLines = 1
            label.heightAnchor.constraint(equalToConstant: 40).isActive = true
            label.widthAnchor.constraint(equalToConstant: 40).isActive = true
            return label
        }
    
        private func setWeekday(index: Int) -> String {
            switch index {
            case 0: return "ПН"
            case 1: return "ВТ"
            case 2: return "СР"
            case 3: return "ЧТ"
            case 4: return "ПТ"
            case 5: return "СБ"
            case 6: return "ВС"
            default: return "??"
            }
        }
        
    }

