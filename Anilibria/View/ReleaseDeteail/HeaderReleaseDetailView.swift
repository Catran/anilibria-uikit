//
//  HeaderReleaseDetailView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit


class LayerContainerView: UILabel {
 
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        refreshGradientLayer()
    }

    private func refreshGradientLayer() {
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.colors = [
            UIColor.systemBackground.withAlphaComponent(0).cgColor,
            UIColor.systemBackground.withAlphaComponent(0.95).cgColor,
            UIColor.systemBackground.withAlphaComponent(1).cgColor
        ]
        gradientLayer.locations = [0, 0.6, 1]
    }
}

class HeaderReleaseDetailView: UIView {
    
    let poster: WebImageView = {
        let view = WebImageView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.image = UIImage(named: "preloadPoster")
        return view
    }()
    
    let gradientView: LayerContainerView = {
       let view = LayerContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let title: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        view.textColor = UIColor.Red.dark
        view.textAlignment = .center
        view.numberOfLines = 0
        view.sizeToFit()
        view.adjustsFontForContentSizeCategory = true
        return view
    }()
    
    let playButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Начать просмотр", for: .normal)
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor.Red.dark
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        view.titleLabel?.adjustsFontForContentSizeCategory = true
        return view
    }()
    
    let showAllSeriaseButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Все серии", for: .normal)
        view.titleLabel?.font = UIFont.preferredFont(forTextStyle: .callout)
        view.titleLabel?.adjustsFontForContentSizeCategory = true
        view.setTitleColor(UIColor.Red.dark, for: .normal)
        return view
    }()
    
    var posterHeightConstraint: NSLayoutConstraint!
    var posterBottomConstraint: NSLayoutConstraint!
    var textBoxBottomConstraint: NSLayoutConstraint!
    
    let titleSpacerBottom: CGFloat = 25
    let playButtonSpacerBottom: CGFloat = 5
    let showAllSeriaseButtonSpacerBottom: CGFloat = 5
    
    init(frame: CGRect, title: String) {
        self.title.text = title
        super.init(frame: frame)
        
        overlayFirstLayer()
        overlaySecondLayer()
        overlayTheadLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func overlayTheadLayer() {
        addSubview(title)
        addSubview(playButton)
        addSubview(showAllSeriaseButton)
        
        //  constraint title
        title.leadingAnchor.constraint(equalTo: gradientView.leadingAnchor, constant: 12).isActive = true
        title.trailingAnchor.constraint(equalTo: gradientView.trailingAnchor, constant: -12).isActive = true
        title.bottomAnchor.constraint(equalTo: playButton.topAnchor, constant: -titleSpacerBottom).isActive = true
        
        //  constraint startPlayButton
        playButton.leadingAnchor.constraint(equalTo: gradientView.leadingAnchor, constant: 25).isActive = true
        playButton.trailingAnchor.constraint(equalTo: gradientView.trailingAnchor, constant: -25).isActive = true
        playButton.bottomAnchor.constraint(equalTo: showAllSeriaseButton.topAnchor, constant: -playButtonSpacerBottom).isActive = true
        
        //  constraint startPlayButton
        
        showAllSeriaseButton.leadingAnchor.constraint(equalTo: gradientView.leadingAnchor, constant: 12).isActive = true
        showAllSeriaseButton.trailingAnchor.constraint(equalTo: gradientView.trailingAnchor, constant: -12).isActive = true
        showAllSeriaseButton.bottomAnchor.constraint(equalTo: gradientView.bottomAnchor, constant: -showAllSeriaseButtonSpacerBottom).isActive = true
    }
    
    func overlaySecondLayer() {
        addSubview(gradientView)
        
        let height = calcGarsienHeight()
        
        //  constraint
        gradientView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        gradientView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        gradientView.heightAnchor.constraint(equalToConstant: height).isActive = true
        textBoxBottomConstraint = gradientView.bottomAnchor.constraint(equalTo: bottomAnchor)
        textBoxBottomConstraint.isActive = true
    }
    
    func overlayFirstLayer() {
        addSubview(poster)
        
        //  constaraint
        poster.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        poster.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        posterBottomConstraint = poster.bottomAnchor.constraint(equalTo: bottomAnchor)
        posterBottomConstraint.isActive = true
        posterHeightConstraint = poster.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0)
        posterHeightConstraint.isActive = true
    }
    
    func calcGarsienHeight() -> CGFloat {
        let titleHieght = title.textHeight(withWidth: bounds.width)
        let playButtonHieght = playButton.titleLabel!.textHeight(withWidth: bounds.width)
        let showAllSeriaseHieght = showAllSeriaseButton.titleLabel!.textHeight(withWidth: bounds.width)
        return CGFloat(titleHieght + playButtonHieght + showAllSeriaseHieght + titleSpacerBottom + playButtonSpacerBottom + showAllSeriaseButtonSpacerBottom + UIScreen.main.bounds.height / 5)
    }
}
