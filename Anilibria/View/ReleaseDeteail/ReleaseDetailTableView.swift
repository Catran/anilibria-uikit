//
//  ReleaseDetailTableView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class ReleaseDetailTableView: UITableView {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let header = tableHeaderView else { return }
        let offsetY = -contentOffset.y
        let view = header as! HeaderReleaseDetailView
        
        view.posterBottomConstraint.constant = offsetY >= 0 ? 0 : -offsetY / 1.4
        //view.textBoxBottomConstraint.constant = offsetY >= 0 ? -offsetY / 2.4 : 0
        view.posterHeightConstraint.constant = max(header.bounds.height, header.bounds.height + offsetY)
        
        header.clipsToBounds = offsetY <= 0
    }
}
