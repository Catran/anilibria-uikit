//
//  FlterViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 09.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol FilterViewModelType: class {
    var isFinishedRelease: Bool { get set }
    var sort: String { get set }
    var years: String { get set }
    var genres: String { get set }
    var seasons: String { get set }
}

class FilterViewModel: FilterViewModelType { 
    var isFinishedRelease: Bool = false
    var sort: String = "1"
    var years: String = ""
    var genres: String = ""
    var seasons: String = ""
}
