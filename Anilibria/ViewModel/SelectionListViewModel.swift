//
//  SelectionListViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 08.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol SelectionListViewModelType: class {
    var list: [String] { get }
    var selectedList: [String] { get set }
    
    func numberOfRows() -> Int
    func cellViewModel(forIndexPath indexPath: IndexPath) -> String
    func fetchList(title: String, completion: @escaping() -> ())
}

class SelectionListViewModel: SelectionListViewModelType {
    
    private let fetcher = NetworkDataFetcher(networking: NetworkService.shared)
    
    var list: [String] = []
    
    var selectedList: [String] = []
    
    func numberOfRows() -> Int {
        return list.count
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> String {
        return list[indexPath.row]
    }
    
    func fetchList(title: String, completion: @escaping() -> ()) {
        if title == "seasons" {
            self.list = ["Зима", "Весна", "Лето", "Осень"]
            completion()
            return
        }
        
        fetcher.getList(title: title) { [weak self] (list) in
            guard let list = list else { return }
            let newList = list.map { (listItem) in
                return listItem
            }
            self?.list = newList
            completion()
        }
    }
}
