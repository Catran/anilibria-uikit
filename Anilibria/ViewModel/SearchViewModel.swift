//
//  SearchViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol SearchViewModelType {
    var releases: [ReleasesType]? { get set }
    
    func numberOfRows() -> Int
    func cellViewModel(forIndexPath indexPath: IndexPath) -> SearchCellViewModelType?
    func detailViewModel(forIndexPath indexPath: IndexPath) -> ReleaseDetailViewModelType?
    
    func fetchReleases(filter: FilterViewModelType, completion: @escaping() -> ())
    func fetchNextReleases(filter: FilterViewModelType, completion: @escaping() -> ())
    
    func fetchSearchTextReleases(title: String, completion: @escaping() -> ())
}

class SearchViewModel: SearchViewModelType {
    
    var releases: [ReleasesType]?
    
    private let fetcher = NetworkDataFetcher(networking: NetworkService.shared)
    
    private var page = 2
    
    func fetchReleases(filter: FilterViewModelType, completion: @escaping() -> ()) {
        fetcher.getReleases(filter: filter) { [weak self] (releases) in
            guard let releases = releases else { return }
            self?.releases = releases.items.map { (releasesItem) in
                return Releases(
                    id: releasesItem.id ?? 0,
                    title: releasesItem.names?[0] ?? "",
                    description: releasesItem.description ?? "",
                    posterUrl: releasesItem.poster ?? "")
            }
            self?.page = 2
            completion()
        }
    }
    
    func fetchNextReleases(filter: FilterViewModelType, completion: @escaping() -> ()) {
        fetcher.getReleases(filter: filter, page: page) { [weak self] (releases) in
            guard let releases = releases else { return }
            let newReleases = releases.items.map { (releasesItem) in
                return Releases(
                    id: releasesItem.id ?? 0,
                    title: releasesItem.names?[0] ?? "",
                    description: releasesItem.description ?? "",
                    posterUrl: releasesItem.poster ?? "")
            }
            self?.page += 1
            self?.releases?.append(contentsOf: newReleases)
            completion()
        }
    }
    
    func fetchSearchTextReleases(title: String, completion: @escaping () -> ()) {
        fetcher.getReleases(title: title) { [weak self] (releases) in
            guard let releases = releases else { return }
            let newReleases = releases.map { (releasesItem) in
                return Releases(
                    id: releasesItem.id ?? 0,
                    title: releasesItem.names?[0] ?? "",
                    description: releasesItem.description ?? "",
                    posterUrl: releasesItem.poster ?? "")
            }
            self?.page = 1
            self?.releases = newReleases
            completion()
        }
    }
    
    func numberOfRows() -> Int {
        return releases?.count ?? 0
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> SearchCellViewModelType? {
        let release = releases?[indexPath.row]
        return SearchCellViewModel(release: release!)
    }
    
    func detailViewModel(forIndexPath indexPath: IndexPath) -> ReleaseDetailViewModelType? {
        let release = releases?[indexPath.row]
        return ReleaseDetailViewModel(id: release!.id, title: release!.title)
    }
}
