//
//  ReleaseDetailViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 22.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol ReleaseDetailViewModelType: class {
    var id: Int { get }
    var title: [String] { get }
    var description: String { get }
    var posterUrl: String { get }
    var type: String { get }
    var geners: [String] { get }
    var voices: [String] { get }
    var year: Int { get }
    var status: String { get }
    var day: Int { get }
    var playlist: [SeriesResponse] { get }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> ReleaseDetailCellViewModelType
    func fetchRelease(id: Int, completion: @escaping() -> ())
}

class ReleaseDetailViewModel: ReleaseDetailViewModelType {
       
    private let fetcher = NetworkDataFetcher(networking: NetworkService.shared)
    
    private var release: DetailReleaseType
    
    var id: Int {
        return release.id
    }
    
    var title: [String] {
        return release.title
    }
    
    var description: String {
        return release.description
    }
    
    var posterUrl: String {
        return release.posterUrl
    }
    
    var type: String {
        return release.type
    }
    
    var geners: [String] {
        return release.geners
    }
    
    var voices: [String] {
        return release.voices
    }
    
    var year: Int {
        return Int(release.year) ?? 0
    }
    
    var status: String {
        return release.status
    }
    
    var day: Int {
        return Int(release.day) ?? 0
    }
    
    var playlist: [SeriesResponse] {
        return release.playlist
    }
    
    init(release: DetailReleaseType) {
        self.release = release
    }
    
    init(id: Int, title: String) {
        self.release = DetailRelease(id: id, title: [title, ""], description: "", posterUrl: "", type: "", geners: [], voices: [], year: "", status: "", day: "", playlist: [])
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> ReleaseDetailCellViewModelType {
        switch(indexPath.row) {
        case 0: return ReleaseDetailCellViewModel(title: "cтатус", description: status)
        case 1: return ReleaseDetailCellViewModel(title: "тип", description: type)
        case 2: return ReleaseDetailCellViewModel(title: "описание", description: description)
        case 3: return ReleaseDetailCellViewModel(title: "колличесво серий", description: "\(playlist.count)")
        case 4: return ReleaseDetailCellViewModel(title: "жанр", description: geners.joined(separator: ", "))
        case 5: return ReleaseDetailCellViewModel(title: "озвучили", description: voices.joined(separator: ", "))
        case 6: return ReleaseDetailCellViewModel(title: "год", description: "\(year)")
        default:
            return ReleaseDetailCellViewModel(title: "", description: "")
        }
    }
    
    func fetchRelease(id: Int, completion: @escaping () -> ()) {
        fetcher.getRelease(id: id) { (release) in
            guard let release = release else { return }
            self.release = DetailRelease(
                id: id,
                title: release.names ?? [],
                description: release.description ?? "",
                posterUrl: release.poster ?? "",
                type: release.type ?? "",
                geners: release.genres ?? [],
                voices: release.voices ?? [],
                year: release.year ?? "",
                status: release.status ?? "",
                day: release.day ?? "0",
                playlist: release.playlist ?? [])
            completion()
        }
    }
}
