//
//  ReleaseDetailCellViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 04.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol ReleaseDetailCellViewModelType: class {
    var title: String { get }
    var description: String { get }
}

class ReleaseDetailCellViewModel: ReleaseDetailCellViewModelType {
    
    private var p_title: String = ""
    private var p_description: String = ""
    
    var title: String {
        return p_title
    }
    
    var description: String {
        return p_description
    }
    
    init(title: String, description: String) {
        self.p_title = title
        self.p_description = description
    }
}
