//
//  SearchCellViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol SearchCellViewModelType: class {
    var title: String { get }
    var description: String { get }
    var posterUrl: String { get }
}

class SearchCellViewModel: SearchCellViewModelType {
    
    private var release: ReleasesType
    
    var title: String {
        return release.title
    }
    
    var description: String {
        return release.description ?? ""
    }
    
    var posterUrl: String {
        let url = release.posterUrl.replacingOccurrences(of: "350x500", with: "270x390")
        return url
    }
    
    init(release: ReleasesType) {
        self.release = release
    }
}
