//
//  RootTabbarController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class RootTabbarController: UITabBarController {
    
    private var controllers: [MainNavigationController] = []
    private var tabbarItem: [(controller: UIViewController.Type, title: String, systemIcon: String)] = [
        (controller: SearchViewController.self, title: "Смотреть", systemIcon: "tv"),
        (controller: SearchViewController.self, title: "Медиатека", systemIcon: "rectangle.stack"),
        (controller: SearchViewController.self, title: "Поиск", systemIcon: "magnifyingglass")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        castomizeTabbarItems()
        setControllers()
        
        viewControllers = controllers
    }
    
    private func castomizeTabbarItems() {
        tabBar.tintColor = UIColor.Red.dark
    }
    
    private func createTabbarItem(title: String, systemIcon: String, tag: Int) -> UITabBarItem {
        let image = UIImage(systemName: systemIcon)
        let tabbarItem = UITabBarItem(title: title, image: image, tag: tag)
        return tabbarItem
    }
    
    private func setControllers() {
        for (index, element) in tabbarItem.enumerated() {
            let controller = element.controller.init()
            controller.tabBarItem = createTabbarItem(title: element.title, systemIcon: element.systemIcon, tag: index)
            controller.title = element.title
            let navigationController = MainNavigationController(rootViewController: controller)
            controllers.append(navigationController)
        }
    }
}
