//
//  SelectedListViewController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 08.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

protocol SelectedListDelegate: class {
    func listDidChcnage(type: String, list: [String])
}

class SelectedListViewController: UITableViewController {
    
    private var type: String = ""
    private var pattern: String = ""
    var viewModel: SelectionListViewModelType?
    weak var delegate: SelectedListDelegate?
    
    init(title: String, type: String, pattern: String) {
        super.init(nibName: nil, bundle: nil)
        
        self.title = title
        self.type = type
        self.pattern = pattern
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsMultipleSelection = true
        tableView.allowsMultipleSelectionDuringEditing = true
        
        tableView.register(CheckableTableViewCell.self, forCellReuseIdentifier: CheckableTableViewCell.reuseId)
        
        self.viewModel = SelectionListViewModel()
        self.viewModel?.fetchList(title: type) {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        listDidChcnage(type: type, list: viewModel!.selectedList)
    }
    
    func listDidChcnage(type:String, list: [String]) {
        delegate?.listDidChcnage(type: type, list: list)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CheckableTableViewCell.reuseId, for: indexPath) as? CheckableTableViewCell
        guard let tableViewCell = cell else { return UITableViewCell() }
        let text = viewModel?.cellViewModel(forIndexPath: indexPath)
        tableViewCell.textLabel?.text = text
        if pattern.contains(text!) {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            viewModel?.selectedList.append(text!)
        }
        return tableViewCell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selectedList.append((viewModel?.list[indexPath.row])!)
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let index = viewModel?.selectedList.firstIndex(of: (viewModel?.list[indexPath.row])!) {
            viewModel?.selectedList.remove(at: index)
        }
    }
}

class CheckableTableViewCell: UITableViewCell {
    
    static let reuseId = "MultiselectTableCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        tintColor = UIColor.Red.dark
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }
}
