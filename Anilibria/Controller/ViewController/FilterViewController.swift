//
//  FilterViewController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 07.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

protocol FilterDelegate: class {
    func retuenValue(filter: FilterViewModelType)
}

class FilterViewController: UIViewController, FilterDelegate {

    private let pickerSortingData = ["по популярности", "по новизне"]
    
    private let filterView = FilterView()
    private var filterViewModel: FilterViewModelType?
    weak var delegate: FilterDelegate?
    
    
    init(filter: FilterViewModelType) {
        super.init(nibName: nil, bundle: nil)
        
        self.filterViewModel = filter
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setFilterValue()
    }
    
    private func setFilterValue() {
        self.filterView.viewModel = self.filterViewModel
    }
    
    private func setup() {
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.isHidden = false
        title = "Фильтры"
        
        view = filterView
        
        self.filterView.switchFinish.addTarget(self, action: #selector(changeStatusFinish(sender:)), for: .allEvents)
        
        self.filterView.toolBarPickerSorting.setItems([UIBarButtonItem(title: "Подтвердить", style: .plain, target: self, action: #selector(donePicker))], animated: true)
        self.filterView.textFieldSorting.inputView = self.filterView.pikerSorting
        self.filterView.textFieldSorting.inputAccessoryView = self.filterView.toolBarPickerSorting
        self.filterView.textFieldSorting.text = pickerSortingData[0]
        self.filterView.textFieldSorting.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(donePicker))
        view.addGestureRecognizer(tap)
        
        //MARK: - UIPickerDelegate, UIPikerDataSource -
        self.filterView.pikerSorting.dataSource = self
        self.filterView.pikerSorting.delegate = self
        
        //MARK: - Add target to years, genres, seasons -
        self.filterView.selectedTextYears.addTarget(self, action: #selector(editSelectedList(sender:)), for: .touchUpInside)
        self.filterView.selectedTextGenres.addTarget(self, action: #selector(editSelectedList(sender:)), for: .touchUpInside)
        self.filterView.selectedTextSeasons.addTarget(self, action: #selector(editSelectedList(sender:)), for: .touchUpInside)
        
        self.filterView.buttonStartSearch.addTarget(self, action: #selector(startSearchByFilter(sender:)), for: .touchUpInside)
        self.filterView.buttonResetFilter.addTarget(self, action: #selector(resetFilter(sender:)), for: .touchUpInside)
    }
    
    @objc func changeStatusFinish(sender: UISwitch) {
        self.filterViewModel?.isFinishedRelease = sender.isOn
    }
    
    @objc func donePicker() {
        self.filterView.textFieldSorting.resignFirstResponder()
    }
    
    @objc func editSelectedList(sender: UIButton) {
        let vc = SelectedListViewController(title: sender.accessibilityHint!, type: sender.accessibilityValue!, pattern: sender.titleLabel?.text ?? "")
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func resetFilter(sender: UIButton) {
        self.filterViewModel?.isFinishedRelease = false
        self.filterViewModel?.sort = "1"
        self.filterViewModel?.years = ""
        self.filterViewModel?.genres = ""
        self.filterViewModel?.seasons = ""
        
        self.filterView.viewModel = self.filterViewModel
        retuenValue(filter: self.filterViewModel!)
    }
    
    @objc func startSearchByFilter(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        retuenValue(filter: self.filterViewModel!)
    }
    
    func retuenValue(filter: FilterViewModelType) {
        delegate?.retuenValue(filter: filter)
    }
}

//MARK: - Protocol support UIPickerDelegate, UIPikerDataSource -
extension FilterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerSortingData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickerSortingData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let selectedRow = pickerSortingData[row]
        self.filterView.textFieldSorting.text = selectedRow
        self.filterViewModel?.sort = row == 0 ? "1" : "0"
    }
}

//MARK: - Protocol support UITextFieldDelegate -
extension FilterViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

//MARK: - Protocol support SelectedListDelegate -
extension FilterViewController: SelectedListDelegate {
    func listDidChcnage(type: String, list: [String]) {
        var text = list.joined(separator: ", ")
        if type == "years" {
            if text.isEmpty {
                text = "Все года..."
                self.filterView.selectedTextYears.setTitle(text, for: .normal)
                self.filterViewModel?.years = ""
            } else {
                self.filterView.selectedTextYears.setTitle(text, for: .normal)
                self.filterViewModel?.years = text
            }
            
        }
        
        if type == "genres" {
            if text.isEmpty {
                text = "Все жанры..."
                self.filterView.selectedTextGenres.setTitle(text, for: .normal)
                self.filterViewModel?.genres = ""
            } else {
                self.filterView.selectedTextGenres.setTitle(text, for: .normal)
                self.filterViewModel?.genres = text
            }
        }
        
        if type == "seasons" {
            if text.isEmpty {
                text = "Все сезоны..."
                self.filterView.selectedTextSeasons.setTitle(text, for: .normal)
                self.filterViewModel?.seasons = ""
            } else {
                self.filterView.selectedTextSeasons.setTitle(text, for: .normal)
                self.filterViewModel?.seasons = text
            }
        }
    }
}
