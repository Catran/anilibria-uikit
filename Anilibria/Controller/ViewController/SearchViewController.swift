//
//  SearchViewController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {

    private var viewModel: SearchViewModelType?
    private let searchController = UISearchController(searchResultsController: nil)
    private var filterViewModel: FilterViewModelType?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        view.backgroundColor = UIColor.white
        
        tableView.register(SearchCellTableViewCell.self, forCellReuseIdentifier: SearchCellTableViewCell.reuseId)
        tableView.register(NotFoundTableViewCell.self, forCellReuseIdentifier: NotFoundTableViewCell.reuseId)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.contentInset.bottom = tabBarController?.tabBar.frame.height ?? 0
        
        self.viewModel = SearchViewModel()
        self.filterViewModel = FilterViewModel()
        self.viewModel?.fetchReleases(filter: self.filterViewModel!) {
            self.tableView.reloadData()
        }
        
        //MARK: - setup searchController -
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Найти анимешку..."
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barStyle = .black
        searchController.searchBar.setPlaceholder(textColor: UIColor.white.withAlphaComponent(0.7))
        searchController.searchBar.setSearchImage(color: .white)
        searchController.searchBar.setClearButton(color: UIColor.white.withAlphaComponent(0.7))
        searchController.searchBar.setValue("Отмена", forKey: "cancelButtonText")
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        //MARK: - filter button -
        let filterShow = UIBarButtonItem(image: UIImage(systemName: "line.horizontal.3.decrease.circle"), style: .done, target: self, action: #selector(filterShow(sender:)))
        filterShow.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = filterShow
    }
    
    @objc func filterShow(sender: UIBarButtonItem) {
        let filterVC = FilterViewController(filter: self.filterViewModel!)
        filterVC.modalPresentationStyle = .pageSheet
        filterVC.delegate = self
        let navC = MainNavigationController(rootViewController: filterVC)
        navC.navigationBar.prefersLargeTitles = false
        self.navigationController?.present(navC, animated: true)
    }
    
    //MARK: - Protocol UITableController -
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y > scrollView.contentSize.height - UIScreen.main.bounds.height - 200 {
            if searchController.searchBar.text!.isEmpty {
                self.viewModel?.fetchNextReleases(filter: self.filterViewModel!) {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    //MARK: - Protocol UITableDataSource, UITableDelegate -
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.numberOfRows() == 0 ? 1 : viewModel!.numberOfRows()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel!.numberOfRows() != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchCellTableViewCell.reuseId, for: indexPath) as? SearchCellTableViewCell
            guard let tableViewCell = cell, let viewModel = viewModel else { return UITableViewCell() }
            let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath)
            tableViewCell.viewModel = cellViewModel
            return tableViewCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NotFoundTableViewCell.reuseId, for: indexPath) as? NotFoundTableViewCell
            guard let tableViewCell = cell else { return UITableViewCell() }
            return tableViewCell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel!.numberOfRows() == 0 ? self.tableView.frame.height - (navigationController?.tabBarController?.tabBar.frame.height ?? 0) : 150
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView.cellForRow(at: indexPath)?.reuseIdentifier != NotFoundTableViewCell.reuseId)
        {
            guard let viewModel = viewModel else { return }
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = ReleaseDetailTableViewController()
            let detailViewModel = viewModel.detailViewModel(forIndexPath: indexPath)
            vc.id = detailViewModel?.id
            vc.title = detailViewModel?.title.first
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if !searchController.searchBar.text!.isEmpty {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SearchViewController.searchContentForSearchText), object: nil)
            self.perform(#selector(SearchViewController.searchContentForSearchText), with: nil, afterDelay: 0.5)
        } else {
            self.viewModel?.fetchReleases(filter: self.filterViewModel!) {
                self.tableView.reloadData()
            }
        }
    }
    
    @objc private func searchContentForSearchText() {
        self.viewModel?.fetchSearchTextReleases(title: searchController.searchBar.text!) {
            self.tableView.reloadData()
        }
    }
    
}

extension SearchViewController: FilterDelegate {
    func retuenValue(filter: FilterViewModelType) {
        
        // scroll to top
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
        self.filterViewModel?.isFinishedRelease = filter.isFinishedRelease
        self.filterViewModel?.sort = filter.sort
        self.filterViewModel?.years = filter.years
        self.filterViewModel?.genres = filter.genres
        self.filterViewModel?.seasons = filter.seasons
        
        self.viewModel?.fetchReleases(filter: self.filterViewModel!) {
            self.tableView.reloadData()
        }
    }
}
