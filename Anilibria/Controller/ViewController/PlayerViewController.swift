//
//  PlayerViewController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 14.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit
import AVKit

class PlayerViewController: AVPlayerViewController {
    
    var avPlayer: AVPlayer?
    var movieURL: String? = nil
    
    init(url: String) {
           super.init(nibName: nil, bundle: nil)
           
           self.movieURL = url
       }
       
       required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let movieURL = movieURL, let url = URL(string: movieURL) else { return }
        let asset = AVAsset(url: url)
        let playerItem = AVPlayerItem(asset: asset)
        self.avPlayer = AVPlayer(playerItem: playerItem)
        self.player = self.avPlayer
        self.player?.play()
    }
}
