//
//  ReleaseDetailTableViewController.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

class ReleaseDetailTableViewController: UITableViewController {

    var id: Int? = nil
    private var viewModel: ReleaseDetailViewModelType? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        if let header = tableView.tableHeaderView {
            header.frame.size.width = tableView.frame.width
            header.frame.size.height = tableView.frame.height - (tabBarController?.tabBar.frame.height ?? 0)
        }
    }
    
    func setup() {
        navigationController?.navigationBar.prefersLargeTitles = false
        
        
        // override tableview
        tableView = ReleaseDetailTableView()
        let headerView = HeaderReleaseDetailView(frame: .zero, title: title ?? "")
        headerView.playButton.addTarget(self, action: #selector(playFirstSeriase), for: .touchUpInside)
        tableView.tableHeaderView = headerView
        
        
        tableView.register(ReleaseDetailTableViewCell.self, forCellReuseIdentifier: ReleaseDetailTableViewCell.reuseId)
        tableView.register(ReleaseWeekdayStatusTableViewCell.self, forCellReuseIdentifier: ReleaseWeekdayStatusTableViewCell.reuseId)
        
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.contentInset.bottom = tabBarController?.tabBar.frame.height ?? 0
        tableView.allowsSelection = false
        
        viewModel = ReleaseDetailViewModel(id: self.id ?? 0, title: self.title ?? "")
        
        if self.id != nil {
            self.viewModel?.fetchRelease(id: self.id!){
                let heeaderView = self.tableView.tableHeaderView as! HeaderReleaseDetailView
                _ = heeaderView.poster.set(imageURL: self.viewModel?.posterUrl)
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func playFirstSeriase(sender: UIButton!) {
        var url = viewModel?.playlist[0].srcHd ?? ""
        url = String(url.split(separator: "?")[0])
        let player = PlayerViewController(url: url)
        self.navigationController?.present(player, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil { return 0 }
        return 7
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel  else { return UITableViewCell() }
        if indexPath.row == 0 && viewModel.status == "В работе" {
            let cell = tableView.dequeueReusableCell(withIdentifier: ReleaseWeekdayStatusTableViewCell.reuseId, for: indexPath) as? ReleaseWeekdayStatusTableViewCell
            guard let tableViewCell = cell else {return UITableViewCell() }
            tableViewCell.viewModel = viewModel
            return tableViewCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReleaseDetailTableViewCell.reuseId, for: indexPath) as? ReleaseDetailTableViewCell
        guard let tableViewCell = cell else { return UITableViewCell() }
        let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath)
        tableViewCell.viewModel = cellViewModel
        return tableViewCell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let viewModel = viewModel  else { return UITableView.automaticDimension }
        if indexPath.row == 0 && viewModel.status == "В работе" {
            return 75
        } else {
            return UITableView.automaticDimension
        }
    }
}
