//
//  UILabel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 04.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

extension UILabel {
    func textHeight(withWidth width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.height(withWidth: width, font: font)
    }

    func attributedTextHeight(withWidth width: CGFloat) -> CGFloat {
        guard let attributedText = attributedText else {
            return 0
        }
        return attributedText.height(withWidth: width)
    }
}
