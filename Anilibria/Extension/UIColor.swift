//
//  UIColor.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit

extension UIColor {
    struct Red {
        static let dark = UIColor(red: 196.0/255.0, green: 3.0/255.0, blue: 4.0/255.0, alpha: 1.0)
    }
}
