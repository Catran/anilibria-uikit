//
//  DetailReleaseModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 04.05.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol DetailReleaseType {
    var id: Int { get }
    var title: [String] { get }
    var description: String { get }
    var posterUrl: String { get }
    var type: String { get }
    var geners: [String] { get }
    var voices: [String] { get }
    var year: String { get }
    var status: String { get }
    var day: String { get }
    var playlist: [SeriesResponse] { get }
}

struct DetailRelease: DetailReleaseType {
    var id: Int
    var title: [String]
    var description: String
    var posterUrl: String
    var type: String
    var geners: [String]
    var voices: [String]
    var year: String
    var status: String
    var day: String
    var playlist: [SeriesResponse]
}
