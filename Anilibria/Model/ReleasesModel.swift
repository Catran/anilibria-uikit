//
//  ReleasesModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 06.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

protocol ReleasesType {
    var id: Int { get }
    var title: String { get }
    var description: String? { get }
    var posterUrl: String { get }
}

struct Releases: ReleasesType {
    var id: Int
    var title: String
    var description: String?
    var posterUrl: String
}
